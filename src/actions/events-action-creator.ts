import { action } from 'typesafe-actions';
import EventModel from '../models/event-model';
import {
  START_LIST_FETCH,
  SUCCESS_LIST_FETCH
} from './../constants/events-constants';

export const startFetchEvents = () => action(START_LIST_FETCH, null);

export const successFetchEvents = (data: EventModel) =>
  action(SUCCESS_LIST_FETCH, { ...data, dirty: false } as EventModel);
