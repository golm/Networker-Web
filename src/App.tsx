import { Breadcrumb, Layout } from 'antd';
import * as React from 'react';
import './App.css';
import SiderComponent from './components/layout-elements/sider';
import EventsList from './features/events/events-list';
const { Content, Footer } = Layout;

class App extends React.Component {
  public render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Layout>
          <SiderComponent />
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Events</Breadcrumb.Item>
              <Breadcrumb.Item>All</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              <EventsList />
            </div>
          </Content>
        </Layout>
        <Footer style={{ textAlign: 'center' }}>Networker</Footer>
      </Layout>
    );
  }
}
export default App;
