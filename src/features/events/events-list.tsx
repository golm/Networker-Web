import { List } from 'antd';
import * as React from 'react';
import EventModel from '../../models/event-model';
import firebaseService from './../../services/firebase-service';
import ListItem from './events-list-item';

export interface InterfaceEventsListProps {
  take?: number;
  skip?: number;
  type?: string;
}

export interface InterfaceEventsListState {
  events: EventModel[];
  eventsLoading: boolean;
}

export default class EventsList extends React.Component<
  InterfaceEventsListProps,
  InterfaceEventsListState
> {
  public state = {
    events: [],
    eventsLoading: false
  };
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.fetchEvents();
  }

  public async fetchEvents() {
    const events = await firebaseService.getEvents();

    this.setState(state => ({
      ...state,
      events,
      eventsLoading: false
    }));
  }

  public render() {
    const { events, eventsLoading } = this.state;
    return (
      <div>
        <List
          loading={eventsLoading}
          renderItem={ListItem}
          dataSource={events}
        />
      </div>
    );
  }
}
