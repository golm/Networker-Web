import { Avatar, List } from 'antd';
import * as React from 'react';
import EventModel from './../../models/event-model';
const { Item } = List;

const ListItem = (item: EventModel) => (
  <Item>
    <Item.Meta
      avatar={
        <Avatar
          src={
            item.images[item.images.length - 1] ||
            'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
          }
        />
      }
      title={item.name}
      description={
        item.description.length > 200
          ? item.description.slice(0, 200) + '...'
          : item.description
      }
    />
  </Item>
);

export default ListItem;
