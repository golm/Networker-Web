const asyncReducerFactory = (name: string) => {
  return (
    state = { data: null, isLoading: false, error: null },
    action: InterfaceAction
  ) => {
    switch (action.type) {
      case `FETCH_${name}_STARTED`:
        return { data: null, isLoading: true, error: null };
      case `FETCH_${name}_SUCCESS`:
        return { data: action.payload, isLoading: false, error: null };
      case `FETCH_${name}_ERROR`:
        return { data: null, isLoading: false, error: action.payload };
      default:
        return state;
    }
  };
};

export default asyncReducerFactory;
