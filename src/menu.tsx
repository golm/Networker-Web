import { Icon, Menu } from 'antd';
import { ClickParam } from 'antd/lib/menu';
import * as React from 'react';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class MenuComponent extends React.Component {
  public state = {
    current: 'mail'
  };
  public handleClick = (e: ClickParam) => {
    this.setState({
      current: e.key
    });
  };
  public render() {
    return (
      <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        mode="inline"
      >
        <Menu.Item key="mail">
          <Icon type="mail" />Navigation One
        </Menu.Item>
        <Menu.Item key="app" disabled={true}>
          <Icon type="appstore" />Navigation Two
        </Menu.Item>
        <SubMenu
          title={
            <span>
              <Icon type="setting" />Navigation Three - Submenu
            </span>
          }
        >
          <MenuItemGroup title="Item 1">
            <Menu.Item key="setting:1">Option 1</Menu.Item>
            <Menu.Item key="setting:2">Option 2</Menu.Item>
          </MenuItemGroup>
          <MenuItemGroup title="Item 2">
            <Menu.Item key="setting:3">Option 3</Menu.Item>
            <Menu.Item key="setting:4">Option 4</Menu.Item>
          </MenuItemGroup>
        </SubMenu>
      </Menu>
    );
  }
}

export default MenuComponent;
