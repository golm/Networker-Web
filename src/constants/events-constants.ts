export const START_LIST_FETCH = '[user] events/start-list-fetch';
export const SUCCESS_LIST_FETCH = '[server] events/start-list-fetch';
export const ERROR_LIST_FETCH = '[server] events/error-list-fetch';
