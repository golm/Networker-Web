import * as firebase from 'firebase';

class FirebaseService {
  // Initialize Firebase
  public config = {
    apiKey: 'AIzaSyCkJprTt1BNP_VcJEs7rGtL2XZYqS6b0Xo',
    authDomain: 'networker-1521383677757.firebaseapp.com',
    databaseURL: 'https://networker-1521383677757.firebaseio.com',
    messagingSenderId: '1033445735061',
    projectId: 'networker-1521383677757',
    storageBucket: 'networker-1521383677757.appspot.com'
  };

  constructor() {
    firebase.initializeApp(this.config);
  }

  /**
   * Fetches all categories from Events collection in firestore
   */
  public getEvents() {
    return firebase
      .firestore()
      .collection('Events')
      .get()
      .then(doc => {
        const events = [];
        doc.forEach(document => {
          const documentData = document.data();
          events.push(documentData);
        });
        return events;
      });
  }

  /**
   * Fetches all categories from categories collection in firestore
   */
  public getCategories() {
    return firebase
      .firestore()
      .collection('categories')
      .get()
      .then(doc => {
        const categories = [];
        doc.forEach(document => categories.push(document.data()));
        return categories;
      });
  }
}

export default new FirebaseService();
