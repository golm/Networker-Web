import { Icon, Layout, Menu } from 'antd';
import * as React from 'react';
import firebaseService from '../../services/firebase-service';

const { SubMenu } = Menu;
const { Sider } = Layout;

export interface InterfaceCategory {
  name: string;
  value: string;
}

export interface InterfaceSiderComponentState {
  collapsed: boolean;
  categories: InterfaceCategory[];
}

class SiderComponent extends React.Component<{}, InterfaceSiderComponentState> {
  public state = {
    categories: [],
    collapsed: false
  };

  public async componentWillMount() {
    const categories = await firebaseService.getCategories();

    this.setState(state => ({
      ...state,
      categories
    }));
  }

  public toggleCollapsed = () => {
    this.setState(state => ({
      collapsed: !state.collapsed
    }));
  };

  public render() {
    const { collapsed, categories } = this.state;
    const styles = {
      headerMenu: {
        color: '#eee',
        fontSize: this.state.collapsed ? '12px' : '24px',
        marginTop: 15,
        padding: '5px'
      }
    };
    return (
      <Sider
        width={256}
        collapsed={collapsed}
        collapsible={true}
        onCollapse={this.toggleCollapsed}
      >
        <div
          style={{
            ...styles.headerMenu,
            textAlign: 'center'
          }}
        >
          {collapsed ? (
            'Networker'
          ) : (
            <div>
              <Icon type="usergroup-add" />Networker
            </div>
          )}
        </div>
        <Menu
          onClick={this.toggleCollapsed}
          style={{ height: '100vh' }}
          mode="inline"
          theme="dark"
        >
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="calendar" />
                <span>Events</span>
              </span>
            }
          >
            {categories.map(category => (
              <Menu.Item key={category.value}>{category.name}</Menu.Item>
            ))}
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="book" />
                <span>My categories</span>
              </span>
            }
          >
            <Menu.Item key="5">Recent categories</Menu.Item>
            <Menu.Item key="6">Bookmarked</Menu.Item>
            <SubMenu key="sub3" title="Loved">
              <Menu.Item key="7">Option 7</Menu.Item>
              <Menu.Item key="8">Option 8</Menu.Item>
            </SubMenu>
          </SubMenu>
          <SubMenu
            key="sub4"
            title={
              <span>
                <Icon type="setting" />
                <span>Settings</span>
              </span>
            }
          >
            <Menu.Item key="9">My profile</Menu.Item>
            <Menu.Item key="10">My prefrences</Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
    );
  }
}

export default SiderComponent;
