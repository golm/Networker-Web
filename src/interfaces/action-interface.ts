interface InterfaceAction {
  payload?: any;
  error?: {
    code: number;
    message: string;
  };
  type: string;
}
