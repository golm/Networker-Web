import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import eventsReducer from './reducers/events-reducer';

const store = createStore(eventsReducer, composeWithDevTools());

export default store;
