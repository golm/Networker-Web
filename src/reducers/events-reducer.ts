import { ActionType, getType } from 'typesafe-actions';
import EventModel from '../models/event-model';
import * as events from './../actions/events-action-creator';

export type EventsAction = ActionType<typeof events>;

export default (state: EventModel[] = [], action: EventsAction) => {
  switch (action.type) {
    case getType(events.startFetchEvents): {
      return [...state];
    }
    default:
      return state;
  }
};
