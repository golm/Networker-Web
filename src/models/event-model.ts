class EventModel {
  description: string;
  images: string[];
  author: string;
  end: Date;
  start: Date;
  location: { latitude: number; longitude: number };
  name: string;
  dirty: boolean;
}

export default EventModel;
